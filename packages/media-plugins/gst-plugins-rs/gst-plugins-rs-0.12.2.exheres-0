# Copyright 2022-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix=https://gitlab.freedesktop.org user=gstreamer tag=${PV} new_download_scheme=true ]
require python [ blacklist=2 multibuild=false ]
require meson [ rust=true ]

SUMMARY="Various GStreamer plugins written in Rust"
HOMEPAGE+=" https://gstreamer.freedesktop.org"
DOWNLOADS="https://gitlab.freedesktop.org/gstreamer/${PN}/-/archive/${PV}/${PNV}.tar.bz2"

LICENCES="
    || ( Apache-2.0 LGPL-2.1 MIT MPL-2.0 )
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gstreamer_plugins:
        closedcaption [[ description = [ Support for closed caption formats (MCC, SCC, EIA-608/CEA-608 and timed text) ] ]]
        dav1d [[ description = [ AV1 decoder based on the dav1d library ] ]]
        gtk4 [[ description = [ GTK4 video sink ] ]]
        onvif [[ description = [ ONVIF fragmented MP4 muxer ] ]]
        spotify [[ description = [ Plugin to read content from Spotify ] ]]
        webp [[ description = [ Support for the Webp image format ] ]]
"

# fills up /var/tmp/paludis/build until running out of space
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/rust:*[>=1.70]
        dev-rust/cargo-c[>=0.9.21]
        virtual/pkg-config
        python_abis:3.8? ( dev-python/tomli[python_abis:3.8] )
        python_abis:3.9? ( dev-python/tomli[python_abis:3.9] )
        python_abis:3.10? ( dev-python/tomli[python_abis:3.10] )
    build+run:
        dev-libs/glib:2[>=2.62]
        dev-libs/libsodium
        media-libs/gstreamer:1.0[>=1.20.0]
        media-plugins/gst-plugins-base:1.0[>=1.20.0]
        gstreamer_plugins:closedcaption? (
            x11-libs/cairo
            x11-libs/pango
        )
        gstreamer_plugins:dav1d? ( media-libs/dav1d:=[>=1.0&<1.3] )
        gstreamer_plugins:gtk4? (
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:opengl]
            x11-libs/graphene:1.0
            x11-libs/gtk:4.0[>=4.6.0]
        )
        gstreamer_plugins:onvif? (
            x11-libs/cairo
            x11-libs/pango
        )
        gstreamer_plugins:webp? ( media-libs/libwebp:= )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Daudiofx=disabled
    -Daws=disabled
    -Dcdg=disabled
    -Dclaxon=disabled
    -Dcsound=disabled
    -Ddoc=disabled
    -Dexamples=disabled
    -Dfallbackswitch=disabled
    -Dffv1=disabled
    -Dfile=enabled
    -Dflavors=disabled
    -Dfmp4=disabled
    -Dgif=disabled
    -Dhlssink3=disabled
    -Dhsv=disabled
    -Dinter=disabled
    -Djson=disabled
    -Dlewton=disabled
    -Dlivesync=disabled
    -Dndi=disabled
    -Dmp4=disabled
    -Dpng=disabled
    -Draptorq=disabled
    -Drav1e=disabled
    -Dregex=disabled
    -Dreqwest=disabled
    -Drtp=disabled
    -Drtsp=disabled
    -Dsodium=enabled
    -Dsodium-source=system
    -Dtests=disabled
    -Dtextahead=disabled
    -Dtextwrap=disabled
    -Dthreadshare=enabled
    -Dtogglerecord=disabled
    -Dtracers=disabled
    -Duriplaylistbin=disabled
    -Dvideofx=disabled
    -Dwebrtc=disabled
    -Dwebrtchttp=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    gstreamer_plugins:closedcaption
    gstreamer_plugins:dav1d
    gstreamer_plugins:gtk4
    gstreamer_plugins:onvif
    gstreamer_plugins:spotify
    gstreamer_plugins:webp
)

src_prepare() {
    meson_src_prepare

    # fix build
    edo sed \
        -e '/rustc.cmd_array/d' \
        -i meson.build
}

