Source/Upstream: Yes, fixed in git master
Reason: SVT-AV1 2.0.0

From 794e1209b6c4ce6edfa4de67b13097c7d0c0cb48 Mon Sep 17 00:00:00 2001
From: Damiano Galassi <damiog@gmail.com>
Date: Thu, 14 Mar 2024 08:07:21 +0100
Subject: [PATCH] contrib: update SVT-AV1 to version 2.0.0.

---
 libhb/common.c                                |  4 -
 libhb/encsvtav1.c                             | 83 ++++++++-----------
 2 files changed, 34 insertions(+), 53 deletions(-)

diff --git a/libhb/common.c b/libhb/common.c
index 53bdea8b362d..d34a47e7eccf 100644
--- a/libhb/common.c
+++ b/libhb/common.c
@@ -1656,10 +1656,6 @@ int hb_video_encoder_get_count_of_analysis_passes(int encoder)
 {
     switch (encoder)
     {
-        case HB_VCODEC_SVT_AV1:
-        case HB_VCODEC_SVT_AV1_10BIT:
-            return 2;
-
         default:
             return 1;
     }
diff --git a/libhb/encsvtav1.c b/libhb/encsvtav1.c
index f6f2e051d17b..99f28ac45221 100644
--- a/libhb/encsvtav1.c
+++ b/libhb/encsvtav1.c
@@ -42,8 +42,6 @@ struct hb_work_private_s
     EbComponentType            *svt_handle;
     EbBufferHeaderType         *in_buf;
 
-    int eos_flag;
-
     struct {
         int64_t duration;
     } frame_info[FRAME_INFO_SIZE];
@@ -293,25 +291,9 @@ int encsvtInit(hb_work_object_t *w, hb_job_t *job)
         job->pass_id == HB_PASS_ENCODE_FINAL)
     {
         hb_interjob_t *interjob = hb_interjob_get(job->h);
-
-        if (job->pass_id == HB_PASS_ENCODE_ANALYSIS)
-        {
-            if (interjob->context == NULL)
-            {
-                param->pass = 1;
-            }
-            else
-            {
-                param->pass = 2;
-            }
-        }
-        else
-        {
-            param->pass = 3;
-        }
-
         param->rc_stats_buffer.buf = interjob->context;
         param->rc_stats_buffer.sz  = interjob->context_size;
+        param->pass = job->pass_id == HB_PASS_ENCODE_ANALYSIS ? 1 : 2;
     }
 
     svt_ret = svt_av1_enc_set_parameter(pv->svt_handle, &pv->enc_params);
@@ -508,68 +490,70 @@ static int send(hb_work_object_t *w, hb_buffer_t *in)
     return 0;
 }
 
-static hb_buffer_t * receive(hb_work_object_t *w)
+static int receive(hb_work_object_t *w, hb_buffer_t **out, int done)
 {
     hb_work_private_t  *pv = w->private_data;
     EbBufferHeaderType *headerPtr;
     EbErrorType svt_ret;
-    hb_buffer_t *out;
+    hb_buffer_t *buf;
 
-    if (pv->eos_flag == 1)
+    svt_ret = svt_av1_enc_get_packet(pv->svt_handle, &headerPtr, done);
+    if (svt_ret == EB_NoErrorEmptyQueue)
     {
-        return NULL;
+        *out = NULL;
+        return 1;
     }
 
-    svt_ret = svt_av1_enc_get_packet(pv->svt_handle, &headerPtr, pv->eos_flag);
-    if (svt_ret == EB_NoErrorEmptyQueue)
+    if (headerPtr->flags & EB_BUFFERFLAG_EOS)
     {
-        return NULL;
+        svt_av1_enc_release_out_buffer(&headerPtr);
+        *out = NULL;
+        return 2;
     }
 
-    out = hb_buffer_init(headerPtr->n_filled_len);
-    if (out == NULL)
+    buf = hb_buffer_init(headerPtr->n_filled_len);
+    if (buf == NULL)
     {
         hb_error("encsvtav1: failed to allocate output packet");
-        return NULL;
+        svt_av1_enc_release_out_buffer(&headerPtr);
+        *out = NULL;
+        return 2;
     }
 
-    memcpy(out->data, headerPtr->p_buffer, headerPtr->n_filled_len);
+    memcpy(buf->data, headerPtr->p_buffer, headerPtr->n_filled_len);
 
-    out->size            = headerPtr->n_filled_len;
-    out->s.start         = headerPtr->pts;
-    out->s.duration      = get_frame_duration(pv);
-    out->s.stop          = out->s.start + out->s.duration;
-    out->s.renderOffset  = headerPtr->dts;
+    buf->size            = headerPtr->n_filled_len;
+    buf->s.start         = headerPtr->pts;
+    buf->s.duration      = get_frame_duration(pv);
+    buf->s.stop          = buf->s.start + buf->s.duration;
+    buf->s.renderOffset  = headerPtr->dts;
 
     // SVT-AV1 doesn't always respect forced keyframes,
     // so always check for chapters
-    hb_chapter_dequeue(pv->chapter_queue, out);
+    hb_chapter_dequeue(pv->chapter_queue, buf);
 
     switch (headerPtr->pic_type)
     {
         case EB_AV1_KEY_PICTURE:
-            out->s.flags |= HB_FLAG_FRAMETYPE_KEY;
+            buf->s.flags |= HB_FLAG_FRAMETYPE_KEY;
             // fall-through
         case EB_AV1_INTRA_ONLY_PICTURE:
-            out->s.frametype = HB_FRAME_IDR;
+            buf->s.frametype = HB_FRAME_IDR;
             break;
         default:
-            out->s.frametype = HB_FRAME_P;
+            buf->s.frametype = HB_FRAME_P;
             break;
     }
 
     if (headerPtr->pic_type != EB_AV1_NON_REF_PICTURE)
     {
-        out->s.flags |= HB_FLAG_FRAMETYPE_REF;
-    }
-
-    if (headerPtr->flags & EB_BUFFERFLAG_EOS)
-    {
-        pv->eos_flag = 1;
+        buf->s.flags |= HB_FLAG_FRAMETYPE_REF;
     }
 
     svt_av1_enc_release_out_buffer(&headerPtr);
-    return out;
+
+    *out = buf;
+    return 0;
 }
 
 static void encode(hb_work_object_t *w, hb_buffer_t *in, hb_buffer_list_t *list)
@@ -577,7 +561,7 @@ static void encode(hb_work_object_t *w, hb_buffer_t *in, hb_buffer_list_t *list)
     send(w, in);
 
     hb_buffer_t *out;
-    while ((out = receive(w)))
+    while (receive(w, &out, 0) == 0)
     {
         hb_buffer_list_append(list, out);
     }
@@ -591,9 +575,10 @@ static void flush(hb_work_object_t *w, hb_buffer_t *in, hb_buffer_list_t *list)
 
     send(w, in);
 
-    while (pv->eos_flag == 0)
+    hb_buffer_t *out = NULL;
+
+    while (receive(w, &out, 1) == 0)
     {
-        hb_buffer_t *out = receive(w);
         hb_buffer_list_append(list, out);
     }
 
