# Copyright 2011 Anders Ladegaard Marchsteiner <alm.anma@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# NOTE: Use a snapshot for compatibility with ffmpeg >=5.0, switch back once there's a working
# release
# require github [ user=dirkvdb release=${PV} suffix=tar.bz2 ]
require github [ user=dirkvdb rev=3db9fe895b2fa656bb40ddb7a62e27604a688171 ]
require cmake
require ffmpeg

SUMMARY="Can be used to create thumbnails for your video files"
DESCRIPTION="
This video thumbnailer can be used to create thumbnails for your video files. The thumbnailer uses
ffmpeg to decode frames from the video files, so supported videoformats depend on the configuration
flags of ffmpeg.
This thumbnailer was designed to be as fast and lightweight as possible. The only dependencies are
ffmpeg, libpng and libjpeg.
The project also includes a C/C++ library that can be used by developers to generate thumbnails in
their projects.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/glib:2
        media-libs/libpng:=
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    '-DENABLE_GIO:BOOL=TRUE'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTS:BOOL=TRUE -DENABLE_TESTS:BOOL=FALSE'
)

