# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Tencent tag=v${PV} ]
require cmake

SUMMARY="A fast JSON parser/generator for C++ with both SAX/DOM style API"

HOMEPAGE+=" http://rapidjson.org/"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
    test:
        dev-cpp/gtest
        dev-util/valgrind
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-fix_class_memaccess_warnings.patch
    "${FILES}"/${PN}-suppress_implicit_fallthrough.patch
    "${FILES}"/system-gtest.patch
    "${FILES}"/152511689bd9b9cdeed6a580479698e13df056b6.patch
    "${FILES}"/0001-valuetest-fix-potential-write-of-terminating-nul-pas.patch
    "${FILES}"/0002-encdedstreamtest-fix-use-after-free-compile-error-wi.patch
    "${FILES}"/${PN}-Avoid-inheritance-from-std-iterator.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DDOC_INSTALL_DIR:PATH=/usr/share/doc/${PNVR}
    -DRAPIDJSON_BUILD_CXX11:BOOL=FALSE
    -DRAPIDJSON_BUILD_CXX17:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'doc RAPIDJSON_BUILD_DOC'
    'examples RAPIDJSON_BUILD_EXAMPLES'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DRAPIDJSON_BUILD_TESTS:BOOL=TRUE -DRAPIDJSON_BUILD_TESTS:BOOL=FALSE'
)

CMAKE_SRC_TEST_PARAMS=(
    # Avoid the valgrind dependency; fails with "Source and destination overlap
    # in memcpy_chk" here with gtest 1.13.0 but not 1.12.1, so let's assume
    # it's a problem in gtest until proven otherwise.
    -E 'valgrind'
)

src_install(){
    cmake_src_install
    ! option examples && edo rm -rf "${IMAGE}"/usr/share/doc/${PNVR}/examples
}

