# Copyright 2012-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=adah1972 release=${PN}_${PV/./_} suffix=tar.gz ]
require alternatives

SUMMARY="An implementation of a Unicode line breaking algorithm"
DESCRIPTION="
An implementation of the line breaking and word breaking algorithms as described in Unicode
Standard Annex 14 and Unicode Standard Annex 29.
"

LICENCES="ZLIB"
SLOT="6"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    run:
        !dev-libs/libunibreak:0[<5.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)

_test_files=(
    # Tests fail with Unicode 15.1 LineTest data (with Brahmic scripts),
    # upstream is aware, but currently has no plans to address this.
    # https://github.com/adah1972/libunibreak/issues/41#issuecomment-1950180208
    #LineBreakTest-2023-08-08.txt
    WordBreakTest-2023-03-31.txt
    GraphemeBreakTest-2023-08-07.txt
)

src_fetch_extra() {
    if expecting_tests ; then
        for file in "${_test_files[@]}" ; do
            if [[ ! -e "${FETCHEDDIR}"/${file} ]] ; then
                edo wget -O "${FETCHEDDIR}"/${file} \
                    https://www.unicode.org/Public/UNIDATA/auxiliary/${file%-????-??-??.txt}.txt
            fi
        done
    fi
}

src_unpack() {
    default

    if expecting_tests ; then
        for file in "${_test_files[@]}" ; do
            edo cp "${FETCHEDDIR}"/${file} \
                "${WORK}"/src/${file%-????-??-??.txt}.txt
        done
    fi
}

src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    default

    local headers=(
        eastasianwidthdef graphemebreak linebreak linebreakdef unibreakbase
        unibreakdef wordbreak
    )
    for header in ${headers[@]} ; do
        arch_dependent_alternatives+=(
            /usr/$(exhost --target)/include/${header}.h ${header}-${SLOT}.h
    )
    done

     arch_dependent_alternatives+=(
        /usr/$(exhost --target)/lib/${PN}.la ${PN}-${SLOT}.la
        /usr/$(exhost --target)/lib/${PN}.so ${PN}-${SLOT}.so
        /usr/$(exhost --target)/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
     )

     alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

