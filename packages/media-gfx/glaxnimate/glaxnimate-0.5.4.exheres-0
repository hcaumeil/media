# Copyright 2022-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=mattbas tag=${PV} suffix=tar.bz2 new_download_scheme=true ] \
    cmake \
    ffmpeg \
    python [ blacklist=2 multibuild=false ] \
    gtk-icon-cache

SUMMARY="Simple vector animation program"
HOMEPAGE+=" https://glaxnimate.mattbas.org"
DOWNLOADS="
    https://gitlab.com/api/v4/projects/19921167/jobs/artifacts/release/raw/${PN}-src.tar.gz?job=tarball -> ${PNV}.tar.gz
    https://github.com/pybind/pybind11/archive/refs/tags/v2.11.1.tar.gz -> ${PNV}-pybind11-2.11.1.tar.gz
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:5
    build+run:
        app-arch/libarchive
        media-gfx/potrace
        sys-libs/zlib
        x11-libs/qtbase:5[gui]
        x11-libs/qtsvg:5
    run:
        kde/breeze:4
    suggestion:
        x11-libs/qtimageformats:5 [[
            description = [ Support for additional image file formats supported by qtimageformats ]
        ]]
"

CMAKE_SOURCE="${WORKBASE}"/${PN}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=FALSE
    -DBUILD_STATIC_LIBS:BOOL=TRUE
    -DCOLOR_WIDGETS_QT_SUFFIX:BOOL=FALSE
    -DGLAXNIMATE_SYSTEM_POTRACE:BOOL=TRUE
    -DMOBILE_UI:BOOL=FALSE
    -DPython3_EXECUTABLE:PATH=${PYTHON}
    -DSCRIPTING_JS:BOOL=FALSE
    -DSNAP_IS_SUCH_A_PAIN:BOOL=FALSE
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
)

src_prepare() {
    cmake_src_prepare

    # do not install bundled breeze icon theme
    edo sed \
        -e '/DIRECTORY icons\/breeze-icons/d' \
        -i data/CMakeLists.txt

    # replace the bundled pybind11 version with a more recent one supporting Python 3.12
    edo rm -rf "${CMAKE_SOURCE}"/external/QtAppSetup/external/pybind11
    edo ln -s "${WORKBASE}"/pybind11-2.11.1 "${CMAKE_SOURCE}"/external/QtAppSetup/external/pybind11
}

src_compile() {
    cmake_src_compile

    # generate translations, last checked: 0.5.4
    edo cmake --build . -j1 --target translations
}

src_install() {
    cmake_src_install

    # buildsystem is a huge mess
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/* "${IMAGE}"/usr/share/
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
}

