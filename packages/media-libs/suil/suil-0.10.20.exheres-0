# Copyright 2013 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="Lightweight C library for loading and wrapping LV2 plugin UIs"
DESCRIPTION="
Suil is a lightweight C library for loading and wrapping LV2 plugin UIs.

Suil makes it possible to load a UI of any toolkit in a host using any other
toolkit (assuming the toolkits are both supported by Suil). Hosts do not need to
build against or link to foreign toolkit libraries to use UIs written with that
toolkit; all the necessary magic is performed by dynamically loaded modules. The
API is designed such that hosts do not need to explicitly support specific
toolkits at all - if Suil supports a particular toolkit, then UIs in that
toolkit will work in all hosts that use Suil automatically.

Suil currently supports every combination of Gtk 2, Qt 4, and X11, e.g. with
Suil a Gtk program can embed a Qt plugin UI without depending on Qt, and a Qt
program can embed a Gtk plugin UI without depending on Gtk. On Windows,
embedding native UIs in Gtk is also supported. I (David Robillard) would be
happy to work with plugin authors to add support for new toolkits, please
contact me if you're interested in writing a plugin UI using a toolkit that is
not yet supported in the LV2 ecosystem.
"
HOMEPAGE="https://drobilla.net/software/${PN}"
DOWNLOADS="https://download.drobilla.net/${PNV}.tar.xz"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="gtk qt5"

# No tests available, last checked: 0.10.20
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/lv2[>=1.18.3]
        x11-libs/libX11
        gtk? ( x11-libs/gtk+:3[>=3.14.0] )
        qt5? (
            x11-libs/qtbase:5[>=5.1.0][gui]
            x11-libs/qtx11extras:5[>=5.1.0]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dcocoa=disabled
    -Ddocs=disabled
    -Dgtk2=disabled
    -Dhtml=disabled
    -Dsinglehtml=disabled
    -Dtitle=Suil
    -Dx11=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gtk gtk3'
    qt5
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

