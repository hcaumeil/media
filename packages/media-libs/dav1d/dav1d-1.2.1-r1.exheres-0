# Copyright 2018-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson alternatives

SUMMARY="Cross-platform AV1 decoder focused on speed and correctness"
HOMEPAGE="https://code.videolan.org/videolan/${PN}"
DOWNLOADS="mirror://videolan/${PN}/${PV}/${PNV}.tar.xz"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/nasm[>=2.14]
    run:
        !media-libs/dav1d:0[<1.2.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Denable_asm=true
    -Denable_docs=false
    -Denable_examples=false
    -Denable_tools=false
    -Dfuzzing_engine=none
    -Dlogging=true
    -Dstack_alignment=0
    -Dtestdata_tests=false
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Denable_tests=true -Denable_tests=false'
)

src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    meson_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

