# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=inkscape suffix=tar.bz2 new_download_scheme=true ]
require cmake

SUMMARY="Easy to use 2D geometry library in C++"
DESCRIPTION="
2Geom is a C++ 2D geometry library geared towards robust processing of
computational geometry data associated with vector graphics. The primary
design consideration is ease of use and clarity.
The library is descended from a set of geometric routines present in Inkscape,
a vector graphics editor based around the Scalable Vector Graphics format, the
most widespread vector graphics interchange format on the Web and a W3C
Recommendation. Due to this legacy, not all parts of the API form a coherent
whole (yet).
Rendering is outside the scope of this library, and it is assumed something
like libcairo or similar is employed for this.  2geom concentrates on higher
level algorithms and geometric computations."

LICENCES="|| ( LGPL-2.1 MPL-1.1 )"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/boost[>=1.60]
        dev-libs/double-conversion:=
        dev-libs/glib:2
        sci-libs/gsl
        x11-libs/cairo
        x11-libs/gtk+:3
    test:
        dev-cpp/gtest
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -D2GEOM_BUILD_SHARED:BOOL=TRUE
    -D2GEOM_BOOST_PYTHON:BOOL=FALSE
    -D2GEOM_CYTHON_BINDINGS:BOOL=FALSE
    -DCMAKE_DISABLE_FIND_PACKAGE_Cython:BOOL=TRUE
    -DWITH_PROFILING:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    "-D2GEOM_TESTING:BOOL=TRUE -D2GEOM_TESTING:BOOL=FALSE"
)

