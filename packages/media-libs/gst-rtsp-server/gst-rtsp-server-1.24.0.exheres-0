# Copyright 2011 Paul Seidler
# Copyright 2019 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="GStreamer based RTSP server"
HOMEPAGE="https://gstreamer.freedesktop.org/modules/${PN}.html"
DOWNLOADS="https://gstreamer.freedesktop.org/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.3] )
    build+run:
        dev-libs/glib:2[>=2.64.0]
        media-libs/gstreamer:1.0[>=${PV}][gobject-introspection?]
        media-plugins/gst-plugins-base:1.0[>=${PV}][gobject-introspection?]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddoc=disabled

    -Drtspclientsink=enabled

    -Dexamples=disabled

    -Dglib-asserts=disabled
    -Dglib-checks=disabled
    -Dgobject-cast-checks=disabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

# A lof of sandbox violations due to networking
RESTRICT="test"

