# Copyright 2021 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="https://git.libcamera.org/libcamera/libcamera.git"
if ! ever is_scm ; then
    SCM_TAG="v${PV}"
fi

require scm-git
require python [ blacklist=2 has_lib=true has_bin=false with_opt=true ]
require meson

export_exlib_phases src_configure

SUMMARY="A complex camera support library"
DESCRIPTION="
Cameras are complex devices that need heavy hardware image processing operations. Control of the
processing is based on advanced algorithms that must run on a programmable processor. This has
traditionally been implemented in a dedicated MCU in the camera, but in embedded devices algorithms
have been moved to the main CPU to save cost. Blurring the boundary between camera devices and Linux
often left the user with no other option than a vendor-specific closed-source solution.

To address this problem the Linux media community has very recently started collaboration with the
industry to develop a camera stack that will be open-source-friendly while still protecting vendor
core IP. libcamera was born out of that collaboration and will offer modern camera support to
Linux-based systems, including traditional Linux distributions, ChromeOS and Android.
"
HOMEPAGE="https://libcamera.org"
DOWNLOADS=""

LICENCES="
    GPL-2
    LGPL-2.1
"
SLOT="0"
MYOPTIONS="
    compliance-tool [[ description = [ Build the compliance test tool lc-compliance ] ]]
    doc
    gstreamer
    gui [[ description = [ Build the Qt5 camera GUI (qcam) ] ]]
    tracing [[ description = [ Build with support for lttng based tracing ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( providers: libunwind llvm-libunwind ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# Some tests require the vivid/vimc drivers to be loaded and need to access /dev/mediaX
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/PyYAML[python_abis:*(-)?]
        dev-python/ply[python_abis:*(-)?]
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            dev-python/Sphinx
            media-gfx/graphviz [[ note = [ For dot ] ]]
        )
        providers:libressl? ( dev-libs/libressl:* )
        providers:openssl? ( dev-libs/openssl:* )
        python? ( dev-libs/pybind11[python_abis:*(-)?] )
    build+run:
        dev-libs/gnutls
        dev-libs/libevent:=
        dev-libs/libyaml
        (
            providers:libunwind? ( dev-libs/libunwind )
            providers:llvm-libunwind? ( sys-libs/llvm-libunwind )
            dev-util/elfutils
        ) [[ note = [ Automagic dependencies of libcamera-base ] ]]
        media-libs/SDL:2
        x11-dri/libdrm
        compliance-tool? ( dev-cpp/gtest )
        gstreamer? (
            dev-libs/glib:2
            media-libs/gstreamer:1.0[>=1.14.0]
            media-plugins/gst-plugins-base:1.0[>=1.14.0]
        )
        gui? (
            media-libs/tiff:=
            x11-libs/qtbase:5[>=5.4][gui]
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        tracing? ( dev-util/lttng-ust )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dandroid=disabled
    -Dcam=enabled
    -Dudev=enabled
    -Dv4l2=true
    -Dwerror=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'compliance-tool lc-compliance'
    'doc documentation'
    gstreamer
    'gui qcam'
    'python pycamera'
    tracing
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtest=true -Dtest=false'
)

libcamera_src_configure() {
    local ipas=(
        ipu3
        vimc
    )
    local pipelines=(
        ipu3
        uvcvideo
        vimc
    )

    if [[ $(exhost --target) == aarch64-*-linux-* ]]; then
        ipas+=(
            rkisp1
            rpi/vc4
        )
        pipelines+=(
            imx8-isi
            rkisp1
            rpi/vc4
            simple
        )
    elif [[ $(exhost --target) == armv7-*-linux-* ]]; then
        ipas+=(
            rpi/vc4
        )
        pipelines+=(
            imx8-isi
            rpi/vc4
            simple
        )
    fi

    meson_src_configure \
        -Dipas=$(echo "${ipas[@]}" | tr ' ' ',') \
        -Dpipelines=$(echo "${pipelines[@]}" | tr ' ' ',')
}

